# Introducción a Inteligencia Artificial y Supercómputo (RIIAA 2020)

La comunidad emergente de Inteligencia Artificial (IA) en la infraestructura de Supercómputo (SC) es fundamental para lograr avances más rápidos, de mayor impacto y que puedan ser escalables. En este taller daremos una introducción teórica-práctica sobre el uso de infraestructura de supercómputo para  problemas de IA. Especialmente abordaremos temas de paralelización y aceleración de  programas con Python y librerias especializadas.


IMPORTANTE: Se recomienda el uso de [Google Colaboratory](https://colab.research.google.com/notebooks/intro.ipynb#recent=true) ya que facilitará mucho la instalación de librerías, así como también que permite el uso de tarjetas gráficas (GPUs) por un determinado periodo de tiempo. Para poder acceder se requerirá de una cuenta de google.

## Objetivos del taller

- Explorar la sinergia que hay entre la IA y el SC. 
- Entender el uso que tiene la infraestructura de SC en aplicaciones de IA.
- Resaltar la importancia de uso del de hardware especializado, principalmente en el área del aprendizaje profundo. 
- Explorar algunas herramientas para acelerar y paralelizar códigos con Python, por ejemplo: 
  - librerías como Numba, Cython y Hummingbird para la acelerar la ejecución de programas.
  - plataformas como TensorFlow, Pytorch para la creación de modelos de IA.
## Colaboratory notebooks del taller

1. **Numba** https://colab.research.google.com/drive/1BwUExIOCGE57xFIIBkzEVyOLeyvRD4TV?usp=sharing
2. **Cython** https://colab.research.google.com/drive/1-N_zfSrOsyxnF1WLynx67qEagVWIFDyh?usp=sharing
3. **Hummingbird** https://colab.research.google.com/drive/1OxE9_33bbMI_3e6TN8_yFCU3cxwX-Otj?usp=sharing

---

##  ¿Cómo trabajar con Google Colaboratory?

Para crear un archivo de Google Colaboratory sigue los siguientes pasos:

1. Ir al siguiente sitio: https://colab.research.google.com/notebooks/intro.ipynb. Aparece lo siguiente:

![](https://gitlab.com/inteligencia-gubernamental-jalisco/riiaa20/uploads/bd23cca5fbb30165aa1928bad0bae0c1/google_colaboratory_main.PNG)

2. Da clic en Archivo > Bloc de notas nuevo.

![](https://gitlab.com/inteligencia-gubernamental-jalisco/riiaa20/uploads/4b2628a5ab41ef5c409eccf9a2963738/google_colaboratory_new.PNG)

3. Posteriormente aparecerá un rectángulo en el cual podrás agregar sentencias como si escribieras en una termina/línea de comandos.

![](https://gitlab.com/inteligencia-gubernamental-jalisco/riiaa20/uploads/fd97489d339be447a40d705a5d7bae34/google_colaboratory_terminal.PNG)



## ¿Cómo trabajar con ambiente de Anaconda/Miniconda?

Para trabajar con Anaconda o Miniconda (versión reducida) se requiere que este sea instalado en nuestros equipos. Para esto realiza lo siguiente:

1. Ir al siguiente sitio: https://docs.conda.io/en/latest/miniconda.html
2. Seleccionar la descarga para el sistema operativo que se esté usando.
3. Una vez descargado, este debe ser instalado.
4. Crear un ambiente a través de los archivos proporcionados en este proyecto (`environment-cpu.yml`, `environment-gpu.yml`). Escribe lo siguiente en la terminal.
 ```bash
 conda env create -f [archivo_ambiente]
  ```
  > ejemplo: conda env create -f environment-cpu.yml
5. Para visualizar los ambientes:
```bash
conda env list
```
6. Cambiar al ambiente previamente creado.
```bash
conda activate [nombre_ambiente]
```
> ejemplo: conda activate riiaa20cpu
___
Dirección de Inteligencia Artificial

Coordinación General de Innovación Gubernamental

Jefatura de Gabinete del Gobierno del Estado de Jalisco
