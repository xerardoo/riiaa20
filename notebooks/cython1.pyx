def cyt_sum(x):
    r = 0
    for i in range(x):
        r += i
    return r

cpdef int typed_cyt_sum(int x):
    cdef int y = 0
    cdef int i
    for i in range(x):
        y += i
    return y

from cython.parallel import prange

cpdef int typed_parall_cyt_sum(int x):
    cdef int y = 0
    cdef int i
    for i in prange(x, nogil=True):
        y += i
    return y
